﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class policjant : Ipolicjant
    {
        public string dzialaj()
        {
            return "policjant podjal akcje";
        }

        public string przerwa()
        {
            return "policjant przerwa";
        }
    }
}
